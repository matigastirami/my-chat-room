$(document).ready(function () {
    var socket = io();

    $("#message-field").on('keyup', function (e) {
        var mensaje;
        if (e.keyCode == 13) {
            mensaje = $('#message-field').val();
        
        sendMessage(mensaje);
        }
    });

    $('#button-send').click(function () {
        var mensaje = $('#message-field').val();
        sendMessage(mensaje);
    })

    $('#message-field').on('input', function (e) {
        socket.emit('user writing');
    })

    socket.on('chat message', function (msg) {
        $('#messages').append($('<li>').text(msg));
    });

    socket.on('user writing', function (msg) {

        $('#escribiendo').show()
        $('#escribiendo').text(msg)

        setTimeout(function () {
            $('#escribiendo').hide()
        }, 1000)

    })

    socket.on("user connected", function (msg) {
        $('#users-connected').append($('<li>').text(msg));

    });

    var sendMessage = function(message){

        if (message.trim() == '') {
            alert("Debe ingresar un mensaje a enviar");
            return;
        }

        socket.emit('chat message', message);

        console.log(socket)

        $('#messages').append($('<li>').text('Yo: ' + message));

        $('#message-field').val('');
    }
})