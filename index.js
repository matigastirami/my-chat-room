
var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('assets'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/views/index.html');
});

io.on('connection', function(socket){ //Acción a tomar cuando un usuario se conecta

  console.log("Usuario conectado")
  console.log(socket.id)
  io.on('connect', function(){
    socket.emit('user connected', socket.id)
  })
  

  socket.on('disconnect', function(){ //Acción a tomar cuando un usuario se desconecta
    console.log('user disconnected');
  });

  socket.on('chat message', function(msg){ //Acción a tomar cuando un usuario emite un mensaje (Desde el jquery se hace un emit con el mensaje deseado, ver index.html)
    console.log('message: ' + msg);
    socket.broadcast.emit('chat message', socket.id + ': ' + msg);
  });

  socket.on('user writing', function(){
    socket.broadcast.emit('user writing', socket.id + ' está escribiendo...');
  })
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});